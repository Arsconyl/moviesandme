import React from 'react'
import { View, StyleSheet, ActivityIndicator, Text } from 'react-native'
import { connect } from 'react-redux'
import FilmList from './FilmList'


class Favorites extends React.Component {

  render() {
    if(this.props.favoritesFilm.length === 0)
      return (<View style={styles.main_container}>
        <Text>Pas de favoris</Text>
      </View>)
      
    else
      return (
        <FilmList
        films={this.props.favoritesFilm}
        navigation={this.props.navigation}
        favoritesList={true}
      />
    )
  }
}

const styles = StyleSheet.create({
  main_container: {
  flex: 1,
  justifyContent: 'center',
  alignItems: 'center',
  fontSize: 26,
},})

const mapStateToProps = state => {
    return {
      favoritesFilm: state.favoritesFilm
    }
  }
  
  export default connect(mapStateToProps)(Favorites)